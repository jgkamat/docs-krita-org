msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-21 03:28+0200\n"
"PO-Revision-Date: 2019-02-27 03:12+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<generated>:1
msgid "Subdivision"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:1
msgid "Grid settings in Krita."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:15
msgid "Grid Settings"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:19
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:21
msgid "Use :menuselection:`Settings --> Configure Krita --> Grid` menu item."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:24
msgid ".. image:: images/preferences/Krita_Preferences_Grid.png"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:25
msgid "Fine tune the settings of the grid-tool grid here."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:28
msgid "Placement"
msgstr "Placement"

#: ../../reference_manual/preferences/grid_settings.rst:30
msgid "The user can set various settings of the grid over here."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:32
msgid "Horizontal Spacing"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:33
msgid ""
"The number in Krita units, the grid will be spaced in the horizontal "
"direction."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid "Vertical Spacing"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid ""
"The number in Krita units, the grid will be spaced in the vertical "
"direction. The images below will show the usage of these settings."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:37
msgid "X Offset"
msgstr "Décalage horizontal"

#: ../../reference_manual/preferences/grid_settings.rst:38
msgid "The number to offset the grid in the X direction."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "Y Offset"
msgstr "Décalage vertical"

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "The number to offset the grid in the Y direction."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:42
msgid ""
"Some examples are shown below, look at the edge of the image to see the "
"offset."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid "Subdivisions"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid ""
"Here the user can set the number of times the grid is subdivided. Some "
"examples are shown below."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:48
msgid "Style"
msgstr "Style"

#: ../../reference_manual/preferences/grid_settings.rst:50
msgid "Main"
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:51
msgid ""
"The user can set how the main lines of the grid are shown. Options available "
"are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""

#: ../../reference_manual/preferences/grid_settings.rst:53
msgid ""
"The user can set how the subdivision lines of the grid are shown. Options "
"available are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
