# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 13:58+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: icons image Kritamouseleft kbd generalsettings images\n"
"X-POFile-SpellExtra: alt ref mouseleft Krita selectionsbasics\n"
"X-POFile-SpellExtra: toolselectrect rectangularselecttool mouseright\n"
"X-POFile-SpellExtra: Kritamouseright\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:66
msgid ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: toolselectrect"
msgstr ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: ferramenta de selecção rectangular"

#: ../../reference_manual/tools/rectangular_select.rst:1
msgid "Krita's rectangular selection tool reference."
msgstr "A referência da ferramenta de selecção rectangular do Krita."

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Selection"
msgstr "Selecção"

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Rectangle"
msgstr "Rectângulo"

#: ../../reference_manual/tools/rectangular_select.rst:11
msgid "Rectangular Selection"
msgstr "Selecção Rectangular"

#: ../../reference_manual/tools/rectangular_select.rst:16
msgid "Rectangular Selection Tool"
msgstr "Ferramenta de Selecção Rectangular"

#: ../../reference_manual/tools/rectangular_select.rst:18
msgid "|toolselectrect|"
msgstr "|toolselectrect|"

#: ../../reference_manual/tools/rectangular_select.rst:20
msgid ""
"This tool, represented by a rectangle with a dashed border, allows you to "
"make :ref:`selections_basics` of a rectangular area. Simply click and drag "
"around the section you wish to select."
msgstr ""
"Esta ferramenta, representada por um rectângulo com um contorno tracejado, "
"permite-lhe criar :ref:`selections_basics` de uma área rectangular. Basta "
"carregar e arrastar a secção que deseja seleccionar."

#: ../../reference_manual/tools/rectangular_select.rst:23
msgid "Hotkeys and Stickykeys"
msgstr "Atalhos e Teclas Fixas"

#: ../../reference_manual/tools/rectangular_select.rst:25
msgid ":kbd:`J` selects this tool."
msgstr ":kbd:`J` selecciona esta ferramenta."

#: ../../reference_manual/tools/rectangular_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` configura a selecção para `substituição` nas opções da ferramenta; "
"este é o modo por omissão."

#: ../../reference_manual/tools/rectangular_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` configura a selecção como `adição` nas opções da ferramenta."

#: ../../reference_manual/tools/rectangular_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` configura a selecção para `subtracção` nas opções da ferramenta."

#: ../../reference_manual/tools/rectangular_select.rst:29
msgid ""
":kbd:`Shift` after starting the selection, constraints it to a perfect "
"square."
msgstr ""
":kbd:`Shift`, após iniciar a selecção, restringe-a a um quadrado perfeito."

#: ../../reference_manual/tools/rectangular_select.rst:30
msgid ""
":kbd:`Ctrl` after starting the selection, makes the selection resize from "
"center."
msgstr ""
":kbd:`Ctrl`, após iniciar a selecção, faz com que a selecção ajuste o "
"tamanho a partir do centro."

#: ../../reference_manual/tools/rectangular_select.rst:31
msgid ":kbd:`Alt` after starting the selection, allows you to move it."
msgstr ":kbd:`Alt`, após iniciar a selecção, permite movê-la."

#: ../../reference_manual/tools/rectangular_select.rst:32
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Shift` + |mouseleft| configura a selecção subsequente para `adição`. "
"Poderá largar a tecla :kbd:`Shift` enquanto arrasta, mas continuará à mesma "
"no modo de 'adição'. O mesmo se aplica aos outros."

#: ../../reference_manual/tools/rectangular_select.rst:33
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt` + |mouseleft| configura a selecção subsequente como `subtracção`."

#: ../../reference_manual/tools/rectangular_select.rst:34
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl` + |mouseleft| configura a selecção subsequente como "
"`substituição`."

#: ../../reference_manual/tools/rectangular_select.rst:35
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt` + |mouseleft| configura a selecção subsequente como "
"`intersecção`."

#: ../../reference_manual/tools/rectangular_select.rst:39
msgid "Hovering over a selection allows you to move it."
msgstr "Se passar o cursor sobre uma selecção permitir-lhe-á movê-la."

#: ../../reference_manual/tools/rectangular_select.rst:40
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Se usar o |mouseright|, irá abrir um menu de selecção rápida que, entre "
"outras coisas, terá a possibilitar de editar a selecção."

#: ../../reference_manual/tools/rectangular_select.rst:44
msgid ""
"So to subtract a perfect square, you do :kbd:`Alt +` |mouseleft|, then "
"release the :kbd:`Alt` key while dragging and press the :kbd:`Shift` key to "
"constrain."
msgstr ""
"Como tal, para subtrair um quadrado perfeito, iria usar o :kbd:`Alt` + |"
"mouseleft|, largando depois a tecla :kbd:`Alt` enquanto arrasta e carregando "
"em :kbd:`Shift` para restringir."

#: ../../reference_manual/tools/rectangular_select.rst:49
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Poderá mudar o comportamento da tecla :kbd:`Alt` para usar como alternativa "
"o :kbd:`Ctrl`, comutando o interruptor na :ref:`general_settings`."

#: ../../reference_manual/tools/rectangular_select.rst:52
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/rectangular_select.rst:54
msgid "Anti-aliasing"
msgstr "Suavização"

#: ../../reference_manual/tools/rectangular_select.rst:55
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Isto indica se são usados contornos leves nas selecções. Algumas pessoas "
"preferem arestas vincadas para as suas selecções."

#: ../../reference_manual/tools/rectangular_select.rst:56
msgid "Width"
msgstr "Largura"

#: ../../reference_manual/tools/rectangular_select.rst:57
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""
"Indica a largura actual. Use o cadeado para forçar a próxima selecção a ter "
"esta largura."

#: ../../reference_manual/tools/rectangular_select.rst:58
msgid "Height"
msgstr "Altura"

#: ../../reference_manual/tools/rectangular_select.rst:59
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""
"Indica a altura actual. Use o cadeado para forçar a próxima selecção a ter "
"esta altura."

#: ../../reference_manual/tools/rectangular_select.rst:61
msgid "Ratio"
msgstr "Taxa"

#: ../../reference_manual/tools/rectangular_select.rst:61
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
"Indica a proporção de tamanho actual. Use o cadeado para forçar a próxima "
"selecção a manter estas proporções."

#: ../../reference_manual/tools/rectangular_select.rst:65
msgid "Round X"
msgstr "Arredondamento em X"

#: ../../reference_manual/tools/rectangular_select.rst:66
msgid "The horizontal radius of the rectangle corners."
msgstr "O raio horizontal dos cantos rectangulares."

#: ../../reference_manual/tools/rectangular_select.rst:67
msgid "Round Y"
msgstr "Arredondamento em Y"

#: ../../reference_manual/tools/rectangular_select.rst:68
msgid "The vertical radius of the rectangle corners."
msgstr "O raio vertical dos cantos rectangulares."
