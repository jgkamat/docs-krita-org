# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-06-17 15:02+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: generalsettings icons similarselecttool ref image\n"
"X-POFile-SpellExtra: Kritamouseleft selectionsbasics kbd Krita images alt\n"
"X-POFile-SpellExtra: toolselectsimilar mouseleft mouseright\n"
"X-POFile-SpellExtra: Kritamouseright\n"

#: ../../<generated>:1
msgid "Fuzziness"
msgstr "Difusão"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: ferramenta de selecção semelhante"

#: ../../reference_manual/tools/similar_select.rst:1
msgid "Krita's similar color selection tool reference."
msgstr "A referência da ferramenta de selecção de cores semelhantes do Krita."

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Selection"
msgstr "Selecção"

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Similar Selection"
msgstr "Selecção de Semelhantes"

#: ../../reference_manual/tools/similar_select.rst:16
msgid "Similar Color Selection Tool"
msgstr "Ferramenta de Selecção de Cores Semelhantes"

#: ../../reference_manual/tools/similar_select.rst:18
msgid "|toolselectsimilar|"
msgstr "|toolselectsimilar|"

#: ../../reference_manual/tools/similar_select.rst:20
msgid ""
"This tool, represented by a dropper over an area with a dashed border, "
"allows you to make :ref:`selections_basics` by selecting a point of color. "
"It will select any areas of a similar color to the one you selected. You can "
"adjust the \"fuzziness\" of the tool in the tool options dock. A lower "
"number will select colors closer to the color that you chose in the first "
"place."
msgstr ""
"Esta ferramenta, representada por um conta-gotas com um contorno tracejado, "
"permite-lhe fazer :ref:`selections_basics` através da selecção de um ponto "
"com uma dada cor. Ele irá seleccionar todas as áreas com uma cor semelhante "
"à que seleccionou. Poderá ajustar a \"difusão\" da ferramenta na área de "
"opções da ferramenta. Um número mais baixo irá seleccionar as cores mais "
"próximas da cor que escolheu em primeiro lugar."

#: ../../reference_manual/tools/similar_select.rst:23
msgid "Hotkeys and Sticky keys"
msgstr "Atalhos e Teclas Fixas"

#: ../../reference_manual/tools/similar_select.rst:25
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` configura a selecção para `substituição` nas opções da ferramenta; "
"este é o modo por omissão."

#: ../../reference_manual/tools/similar_select.rst:26
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ":kbd:`A` configura a selecção como `adição` nas opções da ferramenta."

#: ../../reference_manual/tools/similar_select.rst:27
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` configura a selecção para `subtracção` nas opções da ferramenta."

#: ../../reference_manual/tools/similar_select.rst:28
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
":kbd:`Shift` + |mouseleft| configura a selecção subsequente para `adição`. "
"Poderá largar a tecla :kbd:`Shift` enquanto arrasta, mas continuará à mesma "
"no modo de 'adição'. O mesmo se aplica aos outros."

#: ../../reference_manual/tools/similar_select.rst:29
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt` + |mouseleft| configura a selecção subsequente como `subtracção`."

#: ../../reference_manual/tools/similar_select.rst:30
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl` + |mouseleft| configura a selecção subsequente como "
"`substituição`."

#: ../../reference_manual/tools/similar_select.rst:31
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt` + |mouseleft| configura a selecção subsequente como "
"`intersecção`."

#: ../../reference_manual/tools/similar_select.rst:35
msgid "Hovering over a selection allows you to move it."
msgstr "Se passar o cursor sobre uma selecção permitir-lhe-á movê-la."

#: ../../reference_manual/tools/similar_select.rst:36
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Se usar o |mouseright|, irá abrir um menu de selecção rápida que, entre "
"outras coisas, terá a possibilitar de editar a selecção."

#: ../../reference_manual/tools/similar_select.rst:40
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Poderá mudar o comportamento da tecla :kbd:`Alt` para usar como alternativa "
"o :kbd:`Ctrl`, comutando o interruptor na :ref:`general_settings`."

#: ../../reference_manual/tools/similar_select.rst:43
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/similar_select.rst:46
msgid ""
"This controls whether or not the contiguous selection sees another color as "
"a border."
msgstr ""
"Isto controla se a selecção contígua vê outra cor como uma aresta ou "
"contorno."
