# Spanish translations for docs_krita_org_general_concepts___file_formats___file_png.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_png\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-04-30 20:43+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../general_concepts/file_formats/file_png.rst:1
msgid "The Portable Network Graphics file format in Krita."
msgstr "El formato de archivo de gráficos de red portables en Krita."

#: ../../general_concepts/file_formats/file_png.rst:11
#, fuzzy
#| msgid "\\*.png"
msgid "*.png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:11
#, fuzzy
#| msgid "\\*.png"
msgid "png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "portable network graphics"
msgstr ""

#: ../../general_concepts/file_formats/file_png.rst:17
msgid "\\*.png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:19
#, fuzzy
#| msgid ""
#| ".png, or Portable Network Graphics, is a modern alternative to :ref:"
#| "`file_gif` and with that and :ref:`file_jpg` it makes up the three main "
#| "formats that are widely supported on the internet."
msgid ""
"``.png``, or Portable Network Graphics, is a modern alternative to :ref:"
"`file_gif` and with that and :ref:`file_jpg` it makes up the three main "
"formats that are widely supported on the internet."
msgstr ""
".png, o gráficos de red portables, es una alternativa moderna a :ref:"
"`file_gif`, que junto a este formato y a :ref:`file_jpg` constituyen los "
"tres formatos principales que tienen un amplio uso en Internet."

#: ../../general_concepts/file_formats/file_png.rst:21
#, fuzzy
#| msgid ""
#| "png is a :ref:`lossless <lossless_compression>` file format, which means "
#| "that it is able to maintain all the colors of your image perfectly. It "
#| "does so at the cost of the file size being big, and therefore it is "
#| "recommended to try :ref:`file_jpg` for images with a lot of gradients and "
#| "different colors. Grayscale images will do better in png as well as "
#| "images with a lot of text and sharp contrasts, like comics."
msgid ""
"PNG is a :ref:`lossless <lossless_compression>` file format, which means "
"that it is able to maintain all the colors of your image perfectly. It does "
"so at the cost of the file size being big, and therefore it is recommended "
"to try :ref:`file_jpg` for images with a lot of gradients and different "
"colors. Grayscale images will do better in PNG as well as images with a lot "
"of text and sharp contrasts, like comics."
msgstr ""
"png en un formato de archivo :ref:`lossless <lossless_compression>, lo que "
"significa que es capaz de mantener todos los colores de la imagen "
"perfectamente. Puede hacerlo a cambio de un tamaño de archivo grande, por lo "
"que se recomienda probar :ref:`file_jpg` para imágenes con muchos degradados "
"y colores distintos. Las imágenes en escala de grises son mejores que png "
"también como imágenes con gran cantidad de texto y contrastes agudos, como "
"en los cómics."

#: ../../general_concepts/file_formats/file_png.rst:23
#, fuzzy
#| msgid ""
#| "Like :ref:`file_gif`, png can support indexed color. Unlike :ref:"
#| "`file_gif`, png doesn't support animation. There have been two attempts "
#| "at giving animation support to png, apng and mng, the former is "
#| "unofficial and the latter too complicated, so neither have really taken "
#| "off yet."
msgid ""
"Like :ref:`file_gif`, PNG can support indexed color. Unlike :ref:`file_gif`, "
"PNG doesn't support animation. There have been two attempts at giving "
"animation support to PNG, APNG and MNG, the former is unofficial and the "
"latter too complicated, so neither have really taken off yet."
msgstr ""
"Como :ref:`file_gif`, png puede usar colores indexados. Al contrario que :"
"ref:`file_gif`, png no permite animación. Se han producido dos intentos de "
"permitir el uso de animación en png: apng y mng. El primero no es oficial y "
"el segundo es demasiado complicado, por lo que el uso de ninguno de ellos se "
"ha extendido todavía."

#: ../../general_concepts/file_formats/file_png.rst:25
msgid ""
"Since 4.2 we support saving HDR to PNG as according to the `W3C PQ HDR PNG "
"standard <https://www.w3.org/TR/png-hdr-pq/>`_. To save as such files, "
"toggle :guilabel:`Save as HDR image (Rec. 2020 PQ)`, which will convert your "
"image to the Rec 2020 PQ color space and then save it as a special HDR PNG."
msgstr ""
