# Spanish translations for docs_krita_org_reference_manual___brushes___brush_engines___dyna_brush_engine.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___dyna_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-19 22:09+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Paint Connection"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:1
msgid "The Dyna Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:15
msgid "Dyna Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:18
msgid ".. image:: images/icons/dynabrush.svg"
msgstr ".. image:: images/icons/dynabrush.svg"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:19
msgid ""
"Dyna brush uses dynamic setting like mass and drag to draw strokes. The "
"results are fun and random spinning strokes. To experiment more with this "
"brush you can play with values in 'dynamic settings' section of the brush "
"editor under Dyna Brush."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:23
msgid ""
"This brush engine has been removed in 4.0. This engine mostly had smoothing "
"results that the dyna brush tool has in the toolbox. The stabilizer settings "
"can also give you further smoothing options from the tool options."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:26
msgid "Options"
msgstr "Opciones"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:28
msgid ":ref:`option_size_dyna`"
msgstr ":ref:`option_size_dyna`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:31
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:36
msgid "Brush Size (Dyna)"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:39
msgid "Dynamics Settings"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:41
msgid "Initial Width"
msgstr "Anchura inicial"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:42
msgid "Initial size of the dab."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:43
msgid "Mass"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:44
msgid "How much energy there is in the satellite like movement."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:45
msgid "Drag"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:46
msgid "How close the dabs follow the position of the brush-cursor."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "Width Range"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "How much the dab expands with speed."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:51
msgid "Shape"
msgstr "Forma"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:53
msgid "Diameter"
msgstr "Diámetro"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:54
msgid "Size of the shape."
msgstr "Tamaño de la forma."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:55
msgid "Angle"
msgstr "Ángulo"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:56
msgid "Angle of the shape. Requires Fixed Angle active to work."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:57
msgid "Circle"
msgstr "Círculo"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:58
msgid "Make a circular dab appear."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:59
msgid "Two"
msgstr "Dos"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:60
msgid "Draws an extra circle between other circles."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:61
msgid "Line"
msgstr "Línea"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:62
msgid ""
"Connecting lines are drawn next to each other. The number boxes on the right "
"allows you to set the spacing between the lines and how many are drawn."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:63
msgid "Polygon"
msgstr "Polígono"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:64
msgid "Draws a black polygon as dab."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:65
msgid "Wire"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:66
msgid "Draws the wireframe of the polygon."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:68
msgid "Draws the connection line."
msgstr "Dibuja la línea de conexión."
