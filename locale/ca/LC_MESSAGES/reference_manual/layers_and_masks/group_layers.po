# Translation of docs_krita_org_reference_manual___layers_and_masks___group_layers.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:04+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/layers_and_masks/group_layers.rst:1
msgid "How to use group layers in Krita."
msgstr "Com emprar les capes de grup en el Krita."

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Layers"
msgstr "Capes"

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Groups"
msgstr "Grups"

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Passthrough Mode"
msgstr "Mode curtcircuitat"

#: ../../reference_manual/layers_and_masks/group_layers.rst:17
msgid "Group Layers"
msgstr "Capes de grup"

#: ../../reference_manual/layers_and_masks/group_layers.rst:19
msgid ""
"While working in complex artwork you'll often find the need to group the "
"layers or some portions and elements of the artwork in one unit. Group "
"layers come in handy for this, they allow you to make a segregate the "
"layers, so you can hide these quickly, or so you can apply a mask to all the "
"layers inside this group as if they are one, you can also recursively "
"transform the content of the group... Just drag the mask so it moves to the "
"layer. They are quickly made with the :kbd:`Ctrl + G` shortcut."
msgstr ""
"Mentre treballeu en un treball artístic complex, sovint trobareu la "
"necessitat d'agrupar les capes o algunes parts i elements d'aquest en una "
"unitat. Les Capes de grup són útils per a això, permeten fer una segregació "
"de les capes, de manera que les podreu ocultar ràpidament, o podreu aplicar "
"una màscara a totes les capes dins d'aquest grup com si fossin una, també "
"podreu transformar de manera recursiva el contingut del grup... Simplement "
"arrossegueu la màscara perquè es mogui a la capa. Es creen ràpidament amb la "
"drecera :kbd:`Ctrl + G`."

#: ../../reference_manual/layers_and_masks/group_layers.rst:21
msgid ""
"A thing to note is that the layers inside a group layer are considered "
"separately when the layer gets composited, the layers inside a group are "
"separately composited and then this image is taken in to account when "
"compositing the whole image, while on the contrary, the groups in Photoshop "
"have something called pass-through mode which makes the layer behave as if "
"they are not in a group and get composited along with other layers of the "
"stack. The recent versions of Krita have pass-through mode you can enable it "
"to get similar behavior"
msgstr ""
"Una cosa a tenir en compte és que les capes dins d'una capa de grup es "
"consideraran per separat quan es compongui la capa, les capes dins d'un grup "
"es compondran per separat i després es tindrà en compte aquesta imatge quan "
"es compongui tota la imatge, mentre que al contrari, els grups en el "
"Photoshop tenen quelcom que s'anomena mode curtcircuitat, el qual fa que la "
"capa es comporti com si no estigués en un grup i que es compongui juntament "
"amb les altres capes de la pila. Les versions recents del Krita tenen un "
"mode curtcircuitat que podreu habilitar per obtenir un comportament similar."
