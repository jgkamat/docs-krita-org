# Translation of docs_krita_org_reference_manual___resource_management___resource_brushtips.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:10+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/resource_management/resource_brushtips.rst:0
msgid ".. image:: images/brushes/600px-BSE_Predefined_Window.png"
msgstr ".. image:: images/brushes/600px-BSE_Predefined_Window.png"

#: ../../reference_manual/resource_management/resource_brushtips.rst:1
msgid "Managing brush tips in Krita."
msgstr "Gestionar les puntes de pinzell al Krita."

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
#: ../../reference_manual/resource_management/resource_brushtips.rst:16
msgid "Brushes"
msgstr "Pinzells"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Resources"
msgstr "Recursos"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Brush Tip"
msgstr "Punta del pinzell"

#: ../../reference_manual/resource_management/resource_brushtips.rst:11
msgid "Brush Mask"
msgstr "Màscara del pinzell"

#: ../../reference_manual/resource_management/resource_brushtips.rst:18
msgid ""
"These are the brush tip or textures used in the brush presets. They can be "
"png files or .abr file from Photoshop or .gbr files from gimp."
msgstr ""
"Es tracta de la punta del pinzell o de les textures utilitzades en els "
"pinzells predefinits. Poden ser fitxers PNG o ABR des del Photoshop o GBR "
"des del Gimp."

#: ../../reference_manual/resource_management/resource_brushtips.rst:22
msgid ""
"Currently Krita only import a brush texture from abr file, you have to "
"recreate the brushes by adding appropriate values in size, spacing etc."
msgstr ""
"Actualment, el Krita només importa una textura de pinzell des del fitxer "
"ABR, haureu de recrear els pinzells afegint valors adequats de mida, "
"espaiat, etc."

#: ../../reference_manual/resource_management/resource_brushtips.rst:24
msgid "They can be modified/tagged in the brush preset editor."
msgstr "Es poden modificar/etiquetar a l'editor del pinzell predefinit."

#: ../../reference_manual/resource_management/resource_brushtips.rst:26
msgid "See :ref:`option_brush_tip` for more info."
msgstr "Per a més informació, vegeu :ref:`option_brush_tip`."

#: ../../reference_manual/resource_management/resource_brushtips.rst:29
msgid "Example: Loading a Photoshop Brush (\\*.ABR)"
msgstr "Exemple: carregar un pinzell del Photoshop (\\*.ABR)"

#: ../../reference_manual/resource_management/resource_brushtips.rst:31
msgid ""
"For some time Photoshop has been using the ABR format to compile brushes "
"into a single file.  Krita can read and load .ABR files, although there are "
"certain features. For this example we will use an example of an .ABR file "
"that contains numerous images of types of trees and ferns.  We have two "
"objectives.  The first is to create a series of brushes that we an quickly "
"access from the Brush Presets dock to easily put together a believable "
"forest.  The second is to create a single brush that we can  change on the "
"fly to use for a variety of flora, without the need to have a dedicated "
"Brush Preset for each type."
msgstr ""
"Durant un temps, el Photoshop ha estat utilitzant el format ABR per a "
"compilar els pinzells en un sol fitxer. El Krita pot llegir i carregar els "
"fitxers ABR, tot i que hi ha algunes característiques. Per a aquest exemple, "
"utilitzarem un exemple de fitxer ABR que conté nombroses imatges de tipus "
"d'arbres i falgueres. Tenim dos objectius. El primer és crear una sèrie de "
"pinzells als quals accedirem amb rapidesa des de l'acoblador Pinzells "
"predefinits per a muntar amb facilitat un bosc creïble. El segon és crear un "
"sol pinzell que puguem canviar sobre la marxa per utilitzar-lo per a una "
"varietat de flora, sense la necessitat de disposar d'un Pinzell predefinit "
"per a cada tipus."

#: ../../reference_manual/resource_management/resource_brushtips.rst:33
msgid ""
"First up is download the file (.ZIP, .RAR,...) that contains the .ABR file "
"and any licensing or other notes.  Be sure to read the license if there is "
"one!"
msgstr ""
"En primer lloc es descarrega el fitxer (ZIP, RAR...) que conté el fitxer ABR "
"i qualsevol llicència o altres notes. Assegureu-vos de llegir la llicència "
"si n'hi ha cap!"

#: ../../reference_manual/resource_management/resource_brushtips.rst:34
msgid "Extract the .ABR file into Krita's home directory for brushes."
msgstr ""
"S'extreu el fitxer ABR al directori inicial del Krita per obtenir els "
"pinzells."

#: ../../reference_manual/resource_management/resource_brushtips.rst:35
msgid ""
"In your Brush Presets dock, select one of your brushes that uses the Pixel "
"Brush Engine.  An Ink Pen or solid fill type should do fine."
msgstr ""
"A l'acoblador Pinzells predefinits, seleccioneu un dels vostres pinzells que "
"utilitzin el Motor del pinzell de píxels. Una Ploma de tinta (Ink Pen) o un "
"tipus d'emplenament sòlid ho hauria de fer bé."

# skip-rule: t-acc_obe
#: ../../reference_manual/resource_management/resource_brushtips.rst:36
msgid "Open the Brush Settings Editor (:kbd:`F5` key)."
msgstr "Obriu l'editor per als ajustaments del pinzell (tecla :kbd:`F5`)."

#: ../../reference_manual/resource_management/resource_brushtips.rst:37
msgid ""
"Click on the tab \"Predefined\" next to \"Auto\".  This will change the "
"editor to show a scrollable screen of thumbnail images, most will be black "
"on a white background.  At the bottom of the window are two icons:"
msgstr ""
"Feu clic a la pestanya «Predefinida» al costat de «Automàtic». Això canviarà "
"l'editor per a mostrar una pantalla desplaçable de les imatges en miniatura, "
"la majoria seran negres sobre un fons blanc. A la part inferior de la "
"finestra hi ha dues icones:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:41
msgid ""
"Click on the blue file folder on the left and then navigate to where you "
"saved your .ABR file and open it."
msgstr ""
"Feu clic a la carpeta de fitxers blava de l'esquerra i després navegueu fins "
"a on heu desat el fitxer ABR i obriu-lo."

#: ../../reference_manual/resource_management/resource_brushtips.rst:42
msgid ""
"If everything went fine you will see a number of new thumbnails show up at "
"the bottom of the window.  In our case, they would all be thumbnails "
"representing different types of trees.  Your job now is to decide which of "
"these you want to have as Brush Preset (Just like your Pencil) or you think "
"you'll only use sporadically."
msgstr ""
"Si tot ha anat bé, veureu que apareixen noves miniatures a la part inferior "
"de la finestra. En el nostre cas, seran totes les miniatures que representen "
"els diferents tipus d'arbre. La vostra feina serà decidir quina d'elles "
"voleu tenir com a Pinzell predefinit (igual que el vostre Llapis) o creieu "
"que només l'utilitzareu de forma esporàdica."

#: ../../reference_manual/resource_management/resource_brushtips.rst:43
msgid ""
"Let's say that there is an image of an evergreen tree that we're pretty sure "
"is going to be a regular feature in some of our paintings and we want to "
"have a dedicated brush for it.  To do this we would do the following:"
msgstr ""
"Diguem que hi ha una imatge d'un arbre de fulla perenne que estem segurs que "
"serà una característica habitual en algunes de les nostres pintures i que "
"volem tenir un pinzell dedicat. Per a fer-ho, farem el següent:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:44
msgid "Click on the image of the tree we want"
msgstr "Feu clic sobre la imatge de l'arbre que volem."

#: ../../reference_manual/resource_management/resource_brushtips.rst:45
msgid ""
"Change the name of the brush at the very top of the Brush Editor Settings "
"dialog.  Something like \"Trees - Tall Evergreen\" would be appropriate."
msgstr ""
"Canvieu el nom del pinzell a la part superior del diàleg de configuració per "
"a l'editor de pinzells. Alguna cosa com «Trees - Tall Evergreen» («Arbres: "
"alts i de fulla perenne») seria apropiada."

#: ../../reference_manual/resource_management/resource_brushtips.rst:46
msgid "Click the \"Save to Presets\" button"
msgstr "Feu clic al botó «Desa» per a que es desi com a predefinit."

#: ../../reference_manual/resource_management/resource_brushtips.rst:47
msgid ""
"Now that you have a \"Tall Evergreen\" brush safely saved you can experiment "
"with the settings to see if there is anything you would like to change, for "
"instance, by altering the size setting and the pressure parameter you could "
"set the brush to change the tree size depending on the pressure you were "
"using with your stylus (assuming you have a stylus!)."
msgstr ""
"Ara que tindreu un pinzell «Tall Evergreen» desat de forma segura, podreu "
"experimentar amb els ajustaments per a veure si voleu canviar qualsevol "
"cosa, per exemple, modificant l'ajustament de la mida i el paràmetre de "
"pressió establireu el pinzell per a canviar la mida de l'arbre en funció de "
"la pressió que esteu utilitzant amb el llapis (assumint que teniu un "
"llapis!)."

#: ../../reference_manual/resource_management/resource_brushtips.rst:48
msgid ""
"Once you're satisfied with your brush and its settings you need to do one "
"last thing (but click :guilabel:`Overwrite Brush` first!)"
msgstr ""
"Quan estigueu satisfet amb el vostre pinzell i els seus ajustaments, haureu "
"de fer una última cosa (però primer feu clic a :guilabel:`Sobreescriu el "
"pinzell`)."

#: ../../reference_manual/resource_management/resource_brushtips.rst:50
msgid ""
"It's time now to create the Brush Preview graphic. The simplest and easiest "
"way to do this for a brush of this type is to clear out the ScratchPad using "
"the :guilabel:`Reset` button. Now, center your cursor in the Brush Preview "
"square at the top of the ScratchPad and click once. You should see an image "
"of your texture (in this case it would be the evergreen tree). In order to "
"work correctly though the entire image should fit comfortably within the "
"square. This might mean that you have to tweak the size of the brush. Once "
"you have something you are happy with then click the :guilabel:`Overwrite "
"Brush` button and your brush and its preview image will be saved."
msgstr ""
"Ara és el moment de crear el gràfic per a la vista prèvia del pinzell. La "
"manera més senzilla i fàcil de fer-ho per a un pinzell d'aquest tipus, és "
"netejar el Bloc des de zero amb el botó :guilabel:`Reinicia`. Ara, centreu "
"el cursor al quadre de vista prèvia del pinzell que hi ha a la part superior "
"del Bloc des de zero i feu-hi clic una vegada. Hauríeu de veure una imatge "
"de la textura (en aquest cas seria la de l'arbre de fulla perenne). Per a "
"que funcioni correctament, tota la imatge haurà de cabre còmodament dins del "
"quadrat. Això podria significar que haureu d'ajustar la mida del pinzell. "
"Una vegada tingueu quelcom amb el que esteu content, feu clic al botó :"
"guilabel:`Sobreescriu el pinzell` i es desarà el vostre pinzell i la seva "
"imatge de vista prèvia."

#: ../../reference_manual/resource_management/resource_brushtips.rst:52
msgid ""
"An alternative method that requires a little more work but gives you greater "
"control of the outcome is the following:"
msgstr ""
"Un mètode alternatiu que requereix una mica més de treball, però que dóna "
"més control sobre el resultat és el següent:"

#: ../../reference_manual/resource_management/resource_brushtips.rst:54
msgid ""
"Locate the Brush Preview thumbnail .KPP file in Krita and open it to get a "
"200x200 file that you can edit to your wishes."
msgstr ""
"Localitzeu al Krita el fitxer KPP de la miniatura de vista prèvia del "
"pinzell i obriu-la per obtenir un fitxer de 200x200 que podreu editar."

#: ../../reference_manual/resource_management/resource_brushtips.rst:56
msgid ""
"You're ready to add the next texture!  From here on it's just a matter of "
"wash, rinse and repeat for each texture where you want to create a dedicated "
"Brush Preset."
msgstr ""
"Esteu a punt per afegir la següent textura! A partir d'aquí, només es tracta "
"de rentar, esbandir i repetir-ho per a cada textura de la que vulgueu crear "
"un Pinzell predefinit dedicat."
