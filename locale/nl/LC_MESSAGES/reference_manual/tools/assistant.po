# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 10:20+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../<generated>:1
msgid "Custom Color:"
msgstr "Aangepaste kleur:"

#: ../../<rst_epilog>:42
msgid ""
".. image:: images/icons/assistant_tool.svg\n"
"   :alt: toolassistant"
msgstr ""
".. image:: images/icons/assistant_tool.svg\n"
"   :alt: toolassistant"

#: ../../reference_manual/tools/assistant.rst:1
msgid "Krita's assistant tool reference."
msgstr "Verwijzing naar hulpmiddel Assistent van Krita."

#: ../../reference_manual/tools/assistant.rst:12
msgid "Painting Assistants"
msgstr ""

#: ../../reference_manual/tools/assistant.rst:12
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/assistant.rst:17
msgid "Assistant Tool"
msgstr "Hulpmiddel Assistent"

#: ../../reference_manual/tools/assistant.rst:19
msgid "|toolassistant|"
msgstr ""

#: ../../reference_manual/tools/assistant.rst:21
msgid ""
"Create, edit, and remove drawing assistants on the canvas. There are a "
"number of different assistants that can be used from this tool. The tool "
"options allow you to add new assistants, and to save/load assistants. To add "
"a new assistant, select a type from the tool options and begin clicking on "
"the canvas. Each assistant is created a bit differently. There are also "
"additional controls on existing assistants that allow you to move and delete "
"them."
msgstr ""

#: ../../reference_manual/tools/assistant.rst:23
msgid ""
"The set of assistants on the current canvas can be saved to a \"\\*."
"paintingassistant\" file using the :guilabel:`Save` button in the tool "
"options. These assistants can then be loaded onto a different canvas using "
"the Open button. This functionality is also useful for creating copies of "
"the same drawing assistant(s) on the current canvas."
msgstr ""

#: ../../reference_manual/tools/assistant.rst:25
msgid "Check :ref:`painting_with_assistants` for more information."
msgstr ""

#: ../../reference_manual/tools/assistant.rst:28
msgid "Tool Options"
msgstr "Hulpmiddelopties"

#: ../../reference_manual/tools/assistant.rst:33
msgid "Global Color:"
msgstr "Globale kleur:"

#: ../../reference_manual/tools/assistant.rst:33
msgid ""
"Global color allows you to set the color and opacity of all assistants at "
"once."
msgstr ""

#: ../../reference_manual/tools/assistant.rst:38
msgid ""
"Custom color allows you to set a color and opacity per assistant, allowing "
"for different colors on an assistant. To use this functionality, first "
"'select' an assistant by tapping its move widget. Then go to the tool "
"options docker to see the :guilabel:`Custom Color` check box. Check that, "
"and then use the opacity and color buttons to pick either for this "
"particular assistant."
msgstr ""
