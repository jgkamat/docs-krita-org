# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 10:56+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/file_formats/file_exr.rst:1
msgid "The EXR file format as exported by Krita."
msgstr "Het bestandsformaat EXR zoals geëxporteerd door Krita."

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "EXR"
msgstr "EXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "HDR Fileformat"
msgstr "HDR-bestandsformaat"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "OpenEXR"
msgstr "OpenEXR"

#: ../../general_concepts/file_formats/file_exr.rst:10
msgid "*.exr"
msgstr "*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:15
msgid "\\*.exr"
msgstr "\\*.exr"

#: ../../general_concepts/file_formats/file_exr.rst:17
msgid ""
"``.exr`` is the prime file format for saving and loading :ref:`floating "
"point bit depths <bit_depth>`, and due to the library made to load and save "
"these images being fully open source, the main interchange format as well."
msgstr ""
"``.exr`` is het primaire bestandsformaat voor opslaan en laden van :ref:"
"`floating point bit depths <bit_depth>`, en vanwege de bibliotheek gemaakt "
"om deze afbeeldingen te laden en op te slaan die volledig open-source is, is "
"het ook het hoofduitwisselingsformaat."

#: ../../general_concepts/file_formats/file_exr.rst:19
msgid ""
"Floating point bit-depths are used by the computer graphics industry to "
"record scene referred values, which can be made via a camera or a computer "
"renderer. Scene referred values means that the file can have values whiter "
"than white, which in turn means that such a file can record lighting "
"conditions, such as sunsets very accurately. These EXR files can then be "
"used inside a renderer to create realistic lighting."
msgstr ""
"Floating point bit-depths worden gebruikt door de computergraphicsindustry "
"om scene gerefereerde waarden op te slaan, die gemaakt kunnen worden via een "
"camera- of een computerrenderer. Scene gerefereerde waarden betekent dat het "
"bestand waarden kan hebben witter dan wit, wat op zijn beurt betekent dat "
"zo'n bestand zeer accuraat belichtingscondities kan opnemen zoals "
"zonsondergangen. Deze EXR-bestanden kunnen dan gebruikt worden binnen een "
"renderer om realistische belichting aan te maken."

#: ../../general_concepts/file_formats/file_exr.rst:21
msgid ""
"Krita can load and save EXR for the purpose of paint-over (yes, Krita can "
"paint with scene referred values) and interchange with applications like "
"Blender, Mari, Nuke and Natron."
msgstr ""
"Krita kan EXR laden en opslaan met als doel overschilderen (ja, Krita kan "
"schilderen met scene gerefereerde waarden) en uitwisselen met toepassingen "
"zoals Blender, Mari, Nuke en Natron."
