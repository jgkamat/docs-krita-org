msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:1
msgid "How to use layer styles in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
#: ../../reference_manual/layers_and_masks/layer_styles.rst:17
msgid "Layer Styles"
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer Effects"
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "Layer FX"
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:12
msgid "ASL"
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:19
msgid ""
"Layer styles are effects that are added on top of your layer. They are "
"editable and can easily be toggled on and off. To add a layer style to a "
"layer go to :menuselection:`Layer --> Layer Style`. You can also right-click "
"a layer to access the layer styles."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:22
msgid ""
"When you have the layer styles window up, make sure that the :guilabel:"
"`Enable Effects` item is checked."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:24
msgid ""
"There are a variety of effects and styles you can apply to a layer. When you "
"add a style, your layer docker will show an extra \"Fx\" icon. This allows "
"you to toggle the layer style effects on and off."
msgstr ""

#: ../../reference_manual/layers_and_masks/layer_styles.rst:30
msgid ""
"This feature was added to increase support for :program:`Adobe Photoshop`. "
"The features that are included mirror what that application supports."
msgstr ""
