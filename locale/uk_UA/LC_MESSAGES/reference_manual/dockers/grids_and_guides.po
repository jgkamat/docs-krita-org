# Translation of docs_krita_org_reference_manual___dockers___grids_and_guides.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___grids_and_guides\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:50+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/dockers/grids_and_guides.rst:1
msgid "Overview of the grids and guides docker."
msgstr "Огляд бічної панелі ґраток і напрямних."

#: ../../reference_manual/dockers/grids_and_guides.rst:12
#: ../../reference_manual/dockers/grids_and_guides.rst:63
msgid "Guides"
msgstr "Напрямні"

#: ../../reference_manual/dockers/grids_and_guides.rst:12
msgid "Grid"
msgstr "Ґратка"

#: ../../reference_manual/dockers/grids_and_guides.rst:12
msgid "Isometric Grid"
msgstr "Ізометрична ґратка"

#: ../../reference_manual/dockers/grids_and_guides.rst:12
msgid "Orthogonal Grid"
msgstr "Ортогональна ґратка"

#: ../../reference_manual/dockers/grids_and_guides.rst:17
msgid "Grids and Guides Docker"
msgstr "Бічна панель ґраток і напрямних"

#: ../../reference_manual/dockers/grids_and_guides.rst:19
msgid "The grids and guides docker replaces the :ref:`grid_tool` in Krita 3.0."
msgstr ""
"Ця бічна панель ґраток і напрямних замінила :ref:`grid_tool` у Krita 3.0."

#: ../../reference_manual/dockers/grids_and_guides.rst:21
msgid ""
"This docker controls the look and the visibility of both the Grid and the "
"Guides decorations. It also features a checkbox to quickly toggle snapping "
"on or off."
msgstr ""
"За допомогою цієї бічної панелі можна керувати виглядом та видимістю ґратки "
"та напрямних. Також на панелі передбачено пункт для швидкого вмикання або "
"вимикання прилипання до ґратки чи напрямних."

#: ../../reference_manual/dockers/grids_and_guides.rst:24
msgid "Grids"
msgstr "Ґратки"

#: ../../reference_manual/dockers/grids_and_guides.rst:26
msgid ""
"Grids in Krita can currently only be orthogonal and diagonal. There is a "
"single grid per canvas, and it is saved within the document. Thus it can be "
"saved in a :ref:`templates`."
msgstr ""
"Ґратки у поточній версії Krita можуть бути ортогональними і діагональними. "
"На одному полотні можна використовувати лише одну ґратку, параметри якої "
"зберігаються у документі. Отже, ґратки можна зберігати у :ref:`шаблонах "
"<templates>`."

#: ../../reference_manual/dockers/grids_and_guides.rst:28
msgid "Show Grid"
msgstr "Показати ґратку"

#: ../../reference_manual/dockers/grids_and_guides.rst:29
msgid "Shows or hides the grid."
msgstr "Показує або приховує ґратку."

#: ../../reference_manual/dockers/grids_and_guides.rst:30
msgid "Snap to Grid"
msgstr "Чіплятися до ґратки"

#: ../../reference_manual/dockers/grids_and_guides.rst:31
msgid ""
"Toggles grid snapping on or off. This can also be achieved with the :kbd:"
"`Shift + S` shortcut."
msgstr ""
"Вмикає або вимикає прилипання до ґратки. Для перемикання також можна "
"скористатися комбінацією клавіш :kbd:`Shift + S`."

#: ../../reference_manual/dockers/grids_and_guides.rst:33
msgid "The type of Grid"
msgstr "Тип ґратки"

#: ../../reference_manual/dockers/grids_and_guides.rst:36
msgid "An orthogonal grid."
msgstr "Ортогональна ґратка."

#: ../../reference_manual/dockers/grids_and_guides.rst:38
msgid "X and Y spacing"
msgstr "Інтервал за X та Y"

#: ../../reference_manual/dockers/grids_and_guides.rst:39
msgid "Sets the width and height of the grid in pixels."
msgstr "Встановлює ширину і висоту ґратки у пікселях."

#: ../../reference_manual/dockers/grids_and_guides.rst:41
msgid "Rectangle"
msgstr "Прямокутник"

#: ../../reference_manual/dockers/grids_and_guides.rst:41
msgid "Subdivision"
msgstr "Підпроміжки"

#: ../../reference_manual/dockers/grids_and_guides.rst:41
msgid ""
"Groups cells together as larger squares and changes the look of the lines it "
"contains. A subdivision of 2 will make cells appear twice as big, and the "
"inner lines will become subdivisions."
msgstr ""
"Групує комірки до великих блоків і змінює вигляд ліній, які містяться у "
"блоках. Значення підпроміжку 2 збільшує розмір комірки удвічі, а внутрішні "
"лінії блоку стають лініями допоміжного розбиття."

#: ../../reference_manual/dockers/grids_and_guides.rst:44
msgid "A diagonal grid. Isometric doesn't support snapping."
msgstr "Діагональна ґратка. Для ізометрії прилипання не передбачено."

#: ../../reference_manual/dockers/grids_and_guides.rst:46
msgid "Left and Right Angle"
msgstr "Лівий і правий кути"

#: ../../reference_manual/dockers/grids_and_guides.rst:47
msgid "The angle of the lines. Set both angles to 30° for true isometric."
msgstr ""
"Кут нахилу ліній. Встановіть для обох кутів значення 30°, щоб отримати "
"істинну ізометричну ґратку."

#: ../../reference_manual/dockers/grids_and_guides.rst:49
msgid "Type"
msgstr "Тип"

#: ../../reference_manual/dockers/grids_and_guides.rst:49
msgid "Isometric"
msgstr "Ізометричний"

#: ../../reference_manual/dockers/grids_and_guides.rst:49
msgid "Cell spacing"
msgstr "Інтервал між комірками"

#: ../../reference_manual/dockers/grids_and_guides.rst:49
msgid "Determines how much both sets of lines are spaced."
msgstr "Визначає інтервал між обома наборами ліній."

#: ../../reference_manual/dockers/grids_and_guides.rst:51
msgid "Grid Offset"
msgstr "Зміщення ґратки"

#: ../../reference_manual/dockers/grids_and_guides.rst:52
msgid ""
"Offsets the grid’s starting position from the top-left corner of the "
"document, in pixels."
msgstr ""
"Встановлює відступ початкової позиції ґратки від верхнього лівого кута "
"документа у пікселях."

#: ../../reference_manual/dockers/grids_and_guides.rst:53
msgid "Main Style"
msgstr "Основний стиль"

#: ../../reference_manual/dockers/grids_and_guides.rst:54
msgid "Controls the look of the grid’s main lines."
msgstr "Керує виглядом основних ліній ґратки."

#: ../../reference_manual/dockers/grids_and_guides.rst:56
msgid "Div Style"
msgstr "Стиль поділу"

#: ../../reference_manual/dockers/grids_and_guides.rst:56
msgid "Controls the look of the grid’s “subdivision” lines."
msgstr "Керує виглядом ліній «допоміжного розбиття» ґратки."

#: ../../reference_manual/dockers/grids_and_guides.rst:59
msgid ".. image:: images/dockers/Grid_sudvision.png"
msgstr ".. image:: images/dockers/Grid_sudvision.png"

#: ../../reference_manual/dockers/grids_and_guides.rst:60
msgid ""
"The grid's base size is 64 pixels. With a subdivision of 2, the main grid "
"lines are 128 px away from one another, and the intermediate lines have a "
"different look."
msgstr ""
"Базовим розміром ґратки є 64 пікселі. Із значенням підпроміжків 2, відстань "
"між основними лініями ґратки дорівнюватиме 128 пікселів, а проміжні лінії "
"виглядатимуть інакше."

#: ../../reference_manual/dockers/grids_and_guides.rst:65
msgid ""
"Guides are horizontal and vertical reference lines. You can use them to "
"place and align layers accurately on the canvas."
msgstr ""
"Напрямні — горизонтальні і вертикальні еталонні лінії. Ви можете "
"скористатися ними для точного розташовування та вирівнювання шарів на "
"полотні."

#: ../../reference_manual/dockers/grids_and_guides.rst:68
msgid ".. image:: images/dockers/Guides.jpg"
msgstr ".. image:: images/dockers/Guides.jpg"

#: ../../reference_manual/dockers/grids_and_guides.rst:70
msgid "Creating Guides"
msgstr "Створення напрямних"

#: ../../reference_manual/dockers/grids_and_guides.rst:72
msgid ""
"To create a guide, you need both the rulers and the guides to be visible."
msgstr "Для створення напрямних має бути увімкнено показ лінійок та напрямних."

#: ../../reference_manual/dockers/grids_and_guides.rst:74
msgid "Rulers. (:menuselection:`View --> Show Rulers`)"
msgstr "Лінійки. (:menuselection:`Перегляд --> Показати лінійки`)"

#: ../../reference_manual/dockers/grids_and_guides.rst:75
msgid "Guides.  (:menuselection:`View --> Show Guides`)"
msgstr "Напрямні. (:menuselection:`Перегляд --> Показати напрямні`)"

#: ../../reference_manual/dockers/grids_and_guides.rst:77
msgid ""
"To create a guide, move your cursor over a ruler and drag in the direction "
"of the canvas. A line will appear. Dragging from the left ruler creates a "
"vertical guide, and dragging from the top ruler creates a horizontal guide."
msgstr ""
"Щоб створити напрямку, наведіть курсор на лінійку, затисніть ліву кнопку "
"миші і перетягніть курсор у напрямку полотна. З'явиться лінія. Перетягування "
"з лівої лінійки призведе до створення вертикальної напрямної, а "
"перетягування з верхньої лінійки — до створення горизонтальної напрямної."

#: ../../reference_manual/dockers/grids_and_guides.rst:80
msgid "Editing Guides"
msgstr "Редагування напрямних"

#: ../../reference_manual/dockers/grids_and_guides.rst:82
msgid ""
"Place your cursor above a guide on the canvas. If the guides are not locked, "
"your cursor will change to a double arrow. In that case, click and drag to "
"move the guide. To lock and unlock the guides, open the Grid and Guides "
"Docker. Ensure that the Guides tab is selected. From here you can lock the "
"guides, enable snapping, and change the line style."
msgstr ""
"Наведіть курсор на напрямну на полотні. Якщо напрямні не заблоковано, курсор "
"набуде вигляду подвійної стрілки. Якщо вигляд курсора змінився, натисніть "
"ліву кнопку миші і перетягніть напрямну. Для блокування або розблокування "
"напрямних відкрийте бічну панель ґратки і напрямних. Перейдіть на вкладку :"
"guilabel:`Напрямні`. За допомогою цієї вкладки ви можете блокувати напрямні, "
"вмикати прилипання до напрямних і змінювати стиль ліній напрямних."

#: ../../reference_manual/dockers/grids_and_guides.rst:87
msgid ""
"Currently, it is not possible to create or to move guides to precise "
"positions. The only way to achieve that for now is to zoom in on the canvas, "
"or to use the grid and snapping to place the guide."
msgstr ""
"У поточній версії програми не передбачено створення напрямних у точних "
"позиціях або пересування напрямних у точні позиції. Єдиним способом "
"уточнення розташування напрямних є перетягування на полотні зі збільшеним "
"масштабом або використання ґратки і прилипання для фіксування напрямних."

#: ../../reference_manual/dockers/grids_and_guides.rst:90
msgid "Removing Guides"
msgstr "Вилучення напрямних"

#: ../../reference_manual/dockers/grids_and_guides.rst:92
msgid ""
"Click on the guide you want to remove and drag it outside of the canvas "
"area. When you release your mouse or stylus, the guide will be removed."
msgstr ""
"Наведіть курсор на напрямну, яку ви хочете вилучити, натисніть ліву кнопку "
"миші і перетягніть напрямну за межі полотна. Коли ви відпустите кнопку миші "
"або приберете стило з планшета, напрямну буде вилучено."
